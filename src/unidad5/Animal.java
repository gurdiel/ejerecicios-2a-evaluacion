package unidad5;
import java.time.LocalDate;

public class Animal {
	
	String nombre;
	LocalDate fecha;
	
	public Animal(String nombre,LocalDate fecha){
		
		this.nombre = nombre;
		this.fecha = fecha;
		
	}
	
	public Animal(String nombre) {
		
		this.nombre = nombre;
		this.fecha = LocalDate.now();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	public String toString(){
		return "Nombre: " + this.nombre + " Edad: "
				+ " " + (LocalDate.now().getYear()-this.fecha.getYear()) + " a�os.";
		
	}


}
