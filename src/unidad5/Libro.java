package unidad5;


import java.util.List;

public class Libro {
	
	private String titulo;
	private List<Autor> autores;
	private float precio;
	private int stock;
	
	
	public Libro(String titulo,List<Autor> autores,float precio) {
		
		this.titulo=titulo;
		this.autores=autores;
		this.precio=precio;
		this.stock=0;
		
		
	}
public Libro(String titulo,List<Autor> autores,float precio,int stock) {
		
		this(titulo,autores,precio);
		this.stock=stock;
	}
public float getPrecio() {
	return precio;
}
public void setPrecio(float precio) {
	this.precio = precio;
}
public int getStock() {
	return stock;
}
public void setStock(int stock) {
	this.stock = stock;
}
public String getTitulo() {
	return titulo;
}
public List<Autor> getAutores() {
	return autores;
}

public String toString() {
	String cadena = "";
	
	cadena+=titulo;
	cadena+=" ( ";
	
	for(Autor autor: autores) {
		cadena+=autor.nombre + ",";
		
	}
	cadena+=") " + precio + " �  - " + stock + " "
			+ "u. en stock.";
	
	return cadena;
}
}
