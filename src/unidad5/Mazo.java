package unidad5;

import java.util.ArrayList;

public class Mazo {
	
	ArrayList<Naipe> naipes = new ArrayList<>(); 

	public Mazo() {
		
		palo [] a=palo.values();
		rango[] b = rango.values();
		for(int i=1;i<a.length;i++)
			for(int j=0;j<b.length;j++)
				
				naipes.add(new Naipe(a[i],b[j],j+1));
		
	}

	public String toString() {
		String cadena="";
		for(Naipe a: naipes)
			cadena+= a.getPalo() + " " + a.getValor() + " " + a.getRango() + "\n";
		
			
		return cadena;
	}
}
