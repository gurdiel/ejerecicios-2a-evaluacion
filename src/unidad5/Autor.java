package unidad5;

public class Autor {
	
	String nombre,email,genero;

	public Autor(String nombre,String genero, String email) {
		
		this.nombre = nombre;
		this.email = email;
		this.genero = genero;
		
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public String getGenero() {
		return genero;
	}
	
	public String toString() {
		return this.nombre + "("+ this.genero + ") "
				+ "  " + this.email;
	}

}
