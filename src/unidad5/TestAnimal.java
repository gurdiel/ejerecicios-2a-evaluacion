package unidad5;

import java.time.LocalDate;

public class TestAnimal {

	public static void main(String[] args) {
		Animal animal;
		animal = new Animal("Gato");
		System.out.println(animal.getFecha());
		animal.setFecha(LocalDate.of(2000, 2, 14));
		animal.setNombre("Rafa");
		System.out.println(animal);
		Animal b = new Animal("Perro", LocalDate.of(2008, 8, 25));
		System.out.println(b);

	}

}
