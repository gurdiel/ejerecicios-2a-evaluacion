package repaso2ev;

public class Automovil {
	
	private String modelo;
	private int deposito;
	private float autonomia;
	private double consumo;
	private float kilometraje; // KmTotal
	private float combustible; // totalgastado.
	
	
	public Automovil(String modelo,int deposito,float autonomia,float consumo) {
		this.modelo = modelo;
		this.deposito = deposito;
		this.autonomia = autonomia;
		this.consumo = consumo;
		kilometraje=0;
		combustible=0;
		
		
	}
	public Automovil(String modelo,int deposito,double consumo) {
		
		this.modelo = modelo;
		this.deposito = deposito;
		this.consumo = consumo;
		this.autonomia = deposito;
		kilometraje=0;
		combustible=0;
		
		
	}
	
	public float getKm() {
		return kilometraje;
	}
	
	public float getCombustible() {
		return combustible;
	}
	public void llenarDeposito() {
		
		autonomia=deposito;

	}
	
	public float llenarDeposito(float litros) {
		
		float sobrante=0;
		autonomia = autonomia+litros;
		if(autonomia>deposito) {
			sobrante=autonomia-deposito;
			autonomia = deposito;
		}
			
		
		return sobrante;
	}
	
	public String getMarca() {
		return modelo;
	}
	public float getAutonomia() {
		return autonomia;
	}
	
	public double desplazarse(int km) {
		
		if((km*consumo)>autonomia)
			return -1;
		else {
			kilometraje+=km;
			autonomia-=(km*consumo);
			combustible+=(km*consumo);
			
			return km*consumo;
		}
	}
	
	//SET Y GET VAMOS VIENDO.
	
public String toString() {
		
		String automovil = "Modelo: " + modelo + "\n"+ "Capacidad deposito: " + deposito + "\n"
				+ "Autonom�a: " + autonomia + "\nConsumo: " + consumo + "\nKilometraje total: "
				+ kilometraje + "\nCombustible total: " + combustible;
		
		return automovil;
	}
}
