package repaso2ev;

public class PruebaMatrices {

	public static void main(String[] args) {
		
		int[][]matriz= {{1,5,5,2,4},
						{2,1,4,14,3},
						{3,7,11,2,8},
						{4,8,12,16,4}
						};
		
		int[][]cuadrada1 = Matrices.cuadrada1(6);
		int[][]cuadrada2 = Matrices.cuadrada2(8);
		String[][]matrizPali = Matrices.palindromos(26,4);
		
		Matrices.toString(cuadrada1);
		Matrices.toString(cuadrada2);
		
		if(matrizPali!=null)
		Matrices.toString(matrizPali);
		
		
		System.out.println("La suma m�xima de las matrices de rango 3 dentro de "
				+ "la matriz total es: " + Matrices.max3x3sum(matriz));
		
	}

}
