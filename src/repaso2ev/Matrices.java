package repaso2ev;

public class Matrices {
	
	static int [][] cuadrada1(int dim){
		
		int[][]matriz = new int[dim][dim];
		int contador=1;
		
		for(int i=0;i<dim;i++)
			for(int j=0;j<dim;j++)
			{
				matriz[j][i]=contador;
				contador++;
			}
	
		return matriz;
		
	}
	
static int [][] cuadrada2(int dim){
		
		int[][]matriz = new int[dim][dim];
		int contador=1;
		
		for(int i=0;i<dim;i++)
			if(i%2==0) {
			for(int j=0;j<dim;j++)
			{
				matriz[j][i]=contador;
				contador++;
			}
			}else {
				for(int j=dim-1;j>=0;j--) {
					matriz[j][i]=contador;
					contador++;
				}
			}
	
		return matriz;
		
	}
	
		static String [][] palindromos(int f,int c){
			
			if(f>26 || c>26)
				return null;
			
			String[][]matriz = new String[f][c];
			char []abcdario = {'a','b','c','d','e','f','g','h','i','j','k','l',
					'm','n','�','o','p','q','r','s','t','u','v','w','x','y','z'};
			String pali = "";
			
			
			for(int i=0;i<f;i++)
				for(int j=0;j<c;j++)
				{
					int imasj=0;
					if(i+j>26)
						imasj=(i+j)-abcdario.length;
					else
						imasj=i+j;
						
					pali = " " + abcdario[i] + abcdario[imasj] + abcdario[i];
					matriz[i][j]=pali;
					
				}
		
			return matriz;
			
		}
		
		static int max3x3sum(int [][] matriz){
			
					int max=0;
					
				
					for(int i=0;i<matriz.length-2;i++)
						for(int j=0;j<matriz[i].length-2;j++)
							
						{
								int suma=matriz3x3(i,j,matriz);
								if(suma>max)
									max=suma;
						}
				
			
			return max;
			
			
		}
		
		
		private static int matriz3x3(int i, int j, int[][] matriz) {
			
			int sum=0;
			
			if(i+3>matriz.length || j+3>matriz[i].length)
				return 0;
			
			for(int a=i;a<i+3;a++)
				
				for(int b=j;b<j+3;b++)
					
					sum+=matriz[a][b];
					
					
					
			return sum;
		}

		static void toString(String [][] matriz) {
			for(int i=0;i<matriz.length;i++) {
				for(int j=0;j<matriz[i].length;j++) {
					System.out.print(matriz[i][j] + "\t");
				}
				System.out.println();
			}
			System.out.println();System.out.println();
			System.out.println("--------------------------------------------"
					+ "-----------------------------------------------------");
			System.out.println();System.out.println();
			
		}
		static void toString(int [][] matriz) {
			for(int i=0;i<matriz.length;i++) {
				for(int j=0;j<matriz[i].length;j++) {
					System.out.print(matriz[i][j] + "\t");
				}
				System.out.println();
			}
			
			System.out.println();System.out.println();
			System.out.println("--------------------------------------------"
					+ "-----------------------------------------------------");
			System.out.println();System.out.println();
		}

}
