package repaso2ev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class prueba_automovil {
	
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static Map<String, Automovil> mapa = new HashMap<String, Automovil>();
	
	public static void entrada_datos() throws NumberFormatException, IOException {
		
		System.out.println("N�mero de coches");
		int n = Integer.valueOf(br.readLine());	
		
		for(int i=0;i<n;i++) {
			String cadena = br.readLine();
			String[] vector = cadena.split(" ");	
			Automovil a = new Automovil(vector[0],Integer.parseInt(vector[1]),
					Float.parseFloat(vector[2]),Float.parseFloat(vector[3]));
			mapa.put(vector[0], a);
			
		}
		String s;
			s= br.readLine();
			
			while(!s.equals("fin")) {
				
				String[] desplazar = s.split(" ");
			
					for(String marca: mapa.keySet()) {
						if(marca.compareTo(desplazar[1])==0) {
							
							Automovil p = mapa.get(marca);
							double costeDesplazamiento = p.desplazarse(Integer.parseInt(desplazar[2]));
							if (costeDesplazamiento==-1) {
								System.out.println("Combustible insuficiente "
										+ "para este desplazamiento");
							}else
								System.out.println(p.getMarca() + " " + (float)Math.round(p.getAutonomia() * 100d) / 100d + " " 
										+ (double)Math.round(costeDesplazamiento * 100d) / 100d);
						}
					}
				
				s=br.readLine();
			}
				
			
		//Mientras palabra no sea fin, desplazar, o rellenar o lo que sea...se puede hacer.
		//Como guardo la entrada en un array? puedo guardar objetos Automovil?
		//Ma�ama lo ver�.
	}


	public static void main(String[] args) throws NumberFormatException, IOException {
		
		entrada_datos();
		resumen();
		

	}
	private static void mostrar(Automovil a) {
		System.out.printf("Modelo: %s, Autonom�a: %.2f, KM: %.2f, Combustible: "
				+ "%.2f %n",a.getMarca(),a.getAutonomia(),a.getKm(),a.getCombustible());
	}

	private static void resumen() {
		Iterator<Entry<String, Automovil>> i = mapa.entrySet().iterator();
		
		while(i.hasNext()) {
			Entry<String, Automovil> e = i.next();
			Automovil aux = e.getValue();
			mostrar(aux);
		}
		
		
	}

}
