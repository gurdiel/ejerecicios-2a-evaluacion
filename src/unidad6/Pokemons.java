package unidad6;

import java.util.Random;

public class Pokemons {
	
	private String nombre;
	private int energia;
	private boolean enfermo;
	
	
	public Pokemons(String nombre) {
		
		this.nombre=nombre;
		energia=20;
		enfermo=false;
		
	}
	
	public void comer() {	
		energia+=5;	
		salud();
		humor();
	}
	
	public void dormir() {	
		energia+=2;
		salud();
		
		humor();
	}
	
	public void ejercitar() {
		
		if(!enfermo) {
			energia-=3;
			salud();
		}else
			energia--;
		
		humor();
	}
	
	private void humor() {
		if(!enfermo && energia>=8 && energia<=47 ) {
			System.out.println("Yuhuuuuuu!.");
		}else if(!enfermo && (energia>=5 && energia<8) && (energia>47 && energia<=50) )
			System.out.println("Baaaah!");
		else if(enfermo)
			System.out.println("Oooouuuuchh!");
	}

	public boolean salud() {
		
		if(energia>50 || energia<5 || indigestion() ) {
			System.out.println("Se enferm�." + nombre);
			enfermo=true;
		}
		
		
		return enfermo;
		
	
	}

	private boolean indigestion() {
		boolean aux=false;
		Random rnd = new Random();
		int b = rnd.nextInt();
		if(b>=0 && b<=3)
			aux=true;
		return aux;
	}
	
	public void curar() {
		energia=20;
		enfermo=false;
		System.out.println("Se ha curado, energ�a reestablecida.");
	}
	
	
	public String toString() {
		
		return "Nombre: " + nombre + "\n"
				+ "Energ�a: " + energia;
	}
	

}
