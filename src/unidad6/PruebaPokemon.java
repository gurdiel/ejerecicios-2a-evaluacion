package unidad6;

public class PruebaPokemon {

	public static void main(String[] args) {
		
		/**
		 * Aqu� ir�a un men�, con while y opciones n�mericas, de otra forma
		 * se me complicar�a mucho.
		 * Para probar un poco el objeto he llamado a los metodos directamente.
		 */
		Pokemons p = new Pokemons("Manolo");
		
		p.dormir();
		
		p.ejercitar();
		p.ejercitar();
		p.ejercitar();
		p.ejercitar();
		p.ejercitar();
		p.ejercitar();
		p.curar();
		
		System.out.println(p.toString());

	}

}
