package unidad4;

import java.util.Arrays;
import java.util.Random;

public class Vectores4 {

	public static void main(String[] args) {
		
		Random rnd=new Random();
		int anterior=0;
		int cont=0;
		boolean repetido = false;
		int [] vector=new int [rnd.nextInt(490)+10];
		
		for(int i = 0;i<vector.length;i++) {
			
			vector[i] = rnd.nextInt(200)-100;
			
			if(anterior!=vector[i])
				repetido=false;
			if(anterior==vector[i] && repetido==false) {
				cont++;
				repetido=true;
			}
			anterior=vector[i];
		}
		
		
		if(vector.length<=1050)
			System.out.println(Arrays.toString(vector));
		
		System.out.println(cont);

	}

}
