package unidad4;

import java.util.Scanner;

public class Cadenas3 {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner t = new Scanner(System.in);
		int contador = 0;
		String frase1 = t.nextLine();
		String frase2 = t.nextLine();
		String aux = "";
		for(int i=0;i<=(frase1.length()-frase2.length());i++) {
			aux=(String) frase1.subSequence(i, (i+frase2.length()));
			
			if(aux.compareTo(frase2)==0) {
				contador++;
			}
			
		}
		System.out.println("Se repite: " + contador + " veces.");
		

	}

}
