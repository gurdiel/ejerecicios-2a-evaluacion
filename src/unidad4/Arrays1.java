package unidad4;

import java.util.Arrays;
import java.util.Random;

public class Arrays1 {

	public static void main(String[] args) {
		
		Random rnd = new Random();
		int a=rnd.nextInt(18)+2;
		int b=rnd.nextInt(18)+2;
		int [][] array = new int[a][b];
		
		for(int i=0;i<array.length;i++) {
			for(int j=0;j<array[i].length;j++) {
				
				array[i][j]=rnd.nextInt(100);
			}
		}
		
		vista(array,sumaFila(array),sumaColumna(array));

	}
	static int[] sumaFila(int [][]a) {
		int []v = new int[a.length];
		
		for(int i=0;i<a.length;i++) {
			int suma=0;
			for(int j=0;j<a[i].length;j++) {
				
				suma+=a[i][j];
			}
			v[i]=suma;
		}
		
		
		return v;
		
		
	}
	static int[] sumaColumna(int [][]b) {
		int []v = new int[b[0].length];
		
		for(int i=0;i<b[0].length;i++) {
			int suma=0;
			for(int j=0;j<b.length;j++) {
				
				suma+=b[j][i];
			}
			v[i]=suma;
		}
		
		
		return v;
	}
	static void vista(int [][]arr,int []v1,int []v2) {
		
		for(int i=0;i<arr.length;i++) {
			System.out.print("[");
			for(int j=0;j<arr[i].length;j++) {
				
				System.out.print(arr[i][j] + ", ");
			}
			System.out.print("]");
			System.out.println(" = " + v1[i]);
			System.out.println();
	
		}
		System.out.println(Arrays.toString(v2));

		
	}
	

}
