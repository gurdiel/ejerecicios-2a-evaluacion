package unidad4;

import java.util.Random;
import java.util.Scanner;

public class Vectores2 {

	public static void main(String[] args) {
		
		Random rnd = new Random();
		Scanner t = new Scanner(System.in);
		int aux=0;		
		int [] vector = new int [t.nextInt()];
		long tini = System.currentTimeMillis();
		for(int i = 0;i<vector.length;i++) {
			do {
				
			aux=rnd.nextInt(1999999)-999999;
			
			}while(comprobar(i,aux,vector));
			vector[i]=aux;
		}
		long tfin = System.currentTimeMillis();
		float a = (tfin-tini)/1000f;
		
		long tini1 = System.currentTimeMillis();
		
		int menor=vector[0];
		int mayor=vector[0];
		for(int z=0;z<vector.length;z++) {
			if(vector[z]<menor)
				menor=vector[z];
			if(vector[z]>mayor)
				mayor=vector[z];
		}
		long tfin1 = System.currentTimeMillis();
		float b = (tfin1-tini1)/1000f;
		
		
		System.out.println(vector.length);
		System.out.println( a + " segundos en rellenar sin repetir.");
		System.out.println(mayor + " - " + menor + " = " + (mayor-menor));
		System.out.println( b + " segundos en buscar mayor y menor.");
	}
	
	static boolean comprobar(int i,int aux,int[] array) {
		boolean repe=false;
		
		for(int j=0;j<i;j++) 
			if(array[j]==aux)
				repe=true;
			
		
		return repe;
	}

}
