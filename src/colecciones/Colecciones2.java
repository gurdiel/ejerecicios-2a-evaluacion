package colecciones;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Colecciones2 {

	public static void main(String[] args) {
		
		List<Integer> numeros = new ArrayList<>();
		List<Integer> copia1 = new ArrayList<>();
		
		Random rnd = new Random();
		
		for(int i = 0;i<100;i++) {
			
			numeros.add(rnd.nextInt(100));
		}
		
		for(Integer n: numeros) {
			if(!copia1.contains(n))
				copia1.add(n);
			System.out.print(n + " - ");
		}
		System.out.println();
		
		Iterator ite = copia1.iterator();
		while(ite.hasNext()) {
			System.out.print(ite.next() + " - ");
		}
		System.out.println();
		
		
		Set<Integer> num = new HashSet<>();
		num.addAll(numeros);
		num.forEach(j -> System.out.print(j + " - "));
		
		
		System.out.println();
		System.out.println(numeros);

	}

}
