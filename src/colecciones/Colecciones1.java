package colecciones;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Colecciones1 {

	/**public static void main(String[] args) {
		Scanner s = new Scanner (System.in);
		int a = s.nextInt();
		s.nextLine();//Recoge el intro.
		Set<String> conjunto = new HashSet<String>();
		for(int i=0; i<a;i++) {
			String nombre = s.nextLine();
			conjunto.add(nombre);
		}
		
		System.out.println(conjunto);

	}**/
	public static void main(String[] args) {
		
		Scanner s = new Scanner (System.in);
		int a = s.nextInt();
		s.nextLine();
		List<String> lista = new ArrayList<String>();
		String nombre = "";
		
		for(int i=0;i<a;i++) {
			nombre=s.nextLine();
			if(!lista.contains(nombre))
				lista.add(nombre);
		}
		
		System.out.println(lista);

	}
	
	

}
