package colecciones;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Crea un programa que lea de la entrada est�ndar una l�nea de texto y a 
 * continuaci�n almacene en una colecci�n las palabras que no se repiten y 
 * en otra colecci�n las que s� se repiten. El programa finalizar� mostrando 
 * el contenido de ambas colecciones (hacerlo sin escribir c�digo para iterar).
 * 
 * **/
public class Colecciones3 {

	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		Set<String> almacen = new HashSet<>();
		Set<String> repes = new HashSet<>();
		
		String cadena = teclado.nextLine();
		
		String []v = cadena.split(" ");
		
		for(int i =0;i<v.length;i++) {
			
				if(!almacen.contains(v[i]) && !repes.contains(v[i])) {
						almacen.add(v[i]);	
				}else {
					almacen.remove(v[i]);
					repes.add(v[i]);
				}
		}
		
		System.out.println(Arrays.toString(v));
		
		System.out.println("No repetidos: " + almacen);
		System.out.println("Repetidos: " + repes);

	}

}
